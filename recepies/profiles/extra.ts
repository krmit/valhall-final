/*
 *   Copyright 2019-2020 Magnus Kronnäs
 */
"use strict";
import { command, update, upgrade, install, load, doIt } from "../../lib/shelljs-extra.js";

export default function setup(config:any, password:string, doit:boolean) {
  if(doit) {
	doIt(); 
  }
  const log = config.log;
    
  log.headline("Setup a new user");
  log.log("Read config");
  const guest_path="/etc/valhall/guest-config.yaml";
  const guest_config = load(guest_path);
  log.headline("Install software");
  update();
  upgrade();
  install("bash");
  install("bash-completion");
  install("figlet");
  install("mplayer");

  log.headline("Set upp ssh and sudo");
  log.info("Update the rc bootloader so ssh server always will start on boot up.");
  command("rc-update add sshd");
}
