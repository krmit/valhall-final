/*
 *   Copyright 2019-2020 Magnus Kronnäs
 */
"use strict";
import { command, update, upgrade, install, load, doIt } from "../../lib/shelljs-extra.js";

export default function setup(config:any, password:string, doit:boolean) {
  if(doit) {
	doIt(); 
  }
  const log = config.log;
    
  log.headline("Setup a new user");
  log.log("Read config");
  const guest_path="/etc/valhall/guest-config.yaml";
  const guest_config = load(guest_path);
  log.headline("Install software");
  update();
  upgrade();
  install("sudo");
  install("htop");
  install("openssh");
  install("nano");
  install("bash");
  install("bash-completion");
  log.headline("Install npm software");
  command("npm install -g cowsay")
  log.headline("Set upp ssh and sudo");
  log.info("Update the rc bootloader so ssh server always will start on boot up.");
  command("rc-update add sshd");
  log.info("Create a group sudo");
  command("addgroup sudo")
  log.info("Give the sudo group sudo privileges");
  command("sudo sh -c \"echo '%sudo     ALL=(ALL:ALL) ALL'\" >> /etc/sudoers;")
  log.info("Give user sudo privileges by adding it to sudo group.");
  command(`sudo adduser ${guest_config.alias} sudo;`)
  log.headline("Create useful folders");
  log.info("Create dev folder for git projects.");
  command(`sudo -H -u ${guest_config.alias} sh -c "mkdir ~/dev;"`);
  log.info("Create bin folder for user created or compiled programms");
  command(`sudo -H -u ${guest_config.alias} sh -c "mkdir ~/bin;"`);
  log.info("Create config folder");
  command(`sudo -H -u ${guest_config.alias} sh -c "mkdir -p ~/.config/"`);
  log.headline("Copy files")
  
  log.info("Copy bash config file");
  command(`sudo -H -u ${guest_config.alias} sh -c "cp /etc/valhall/bashrc ~/.bashrc;"`); 
  log.info("Created profile file to load .bashrc");
  command(`sh -c "echo 'source ~/.bashrc' > /home/${guest_config.alias}/.profile"`);
  
  log.info("Copy micro config file");
  command(`sudo -H -u ${guest_config.alias} sh -c "mkdir -p ~/.config/micro"`);
  command(`sudo -H -u ${guest_config.alias} sh -c "cp /etc/valhall/bindings.js ~/.config/micro/"`);

  log.info("Change passwd file so bash is standard for user.");
  command("sed -i -- '$s/\\/bin\\/ash/\\/bin\\/bash/g' /etc/passwd")

 /* log.headline("Install software from git");
  command(`sudo -H -u ${guest_config.alias} sh -c /etc/valhall/install-mimer-editor.sh`);
  log.info("Copy med editor config file");
  command(`sudo -H -u ${guest_config.alias} sh -c "mkdir -p ~/.config/mimer-editor/"`);
  command(`sudo -H -u ${guest_config.alias} sh -c "cp /etc/editor-config.yml ~/.config/mimer-editor/config.yml;"`);
  command(`sudo -H -u ${guest_config.alias} sh -c "cp /etc/editor-config.yml ~/.config/mimer-editor/config-old.yml;"`);*/
  log.headline("Create ssh keys.");
  log.info("Create ssh map");
  command(`sudo -H -u ${guest_config.alias} sh -c "mkdir ~/.ssh;"`)
  log.info("Copy public key for host to root user on guest.");
  command(`sudo -H -u ${guest_config.alias} sh -c "cat /etc/valhall/id_rsa.pub >> ~/.ssh/authorized_keys;"`)
  log.info("Generate ssh keys");
  command(`sudo -H -u ${guest_config.alias} sh -c "ssh-keygen -f ~/.ssh/id_rsa -q -P \\"\\""`);
  log.headline("Reboot the system");
  log.warn("Reboot!");
  command("reboot");
}

/*
msgInfo "Get password from parameter";
PASSWORD=$1;

msgHeadline 
apk update;
apk upgrade;
apk add htop;
apk add sudo;
apk add openssh;
apk add git;
apk add nodejs;
apk add nano;
apk add neovim;

msgHeadline "Set upp ssh and sudo";
msgInfo "Update the rc bootloader so ssh server always will start on boot up.";
rc-update add sshd

msgInfo "Create a group sudo";
addgroup sudo

msgInfo "Give the sudo group sudo privileges";
sh -c "sudo echo '%sudo     ALL=(ALL:ALL) ALL' >> /etc/sudoers";

msgHeadline "Create a user";
msgInfo "Create a user with username: $VALHALL_USER";
msgInfo "Set password.";
#Could be more secure, 
export VALHALL_USER;
export PASSWORD;
a

msgInfo "Give user sudo privileges by adding it to sudo group.";
adduser $VALHALL_USER sudo;

msgHeadline "Create useful folders";
msgInfo "Create dev folder for git projects.";
mkdir /home/$VALHALL_USER/dev; chown $VALHALL_USER:$VALHALL_USER /home/$VALHALL_USER/dev
msgInfo "Create bin folder for user created or compiled programms.";
mkdir /home/$VALHALL_USER/bin; chown $VALHALL_USER:$VALHALL_USER /home/$VALHALL_USER/bin

msgInfo "Copy files";
cp /etc/valhall/.profile /home/$VALHALL_USER/.profile;
chmod 700 /home/$VALHALL_USER/.profile;
chown $VALHALL_USER:$VALHALL_USER /home/$VALHALL_USER/.profile;

msgHeadline "Create ssh keys.";
mkdir /home/$VALHALL_USER/.ssh; chown $VALHALL_USER:$VALHALL_USER /home/$VALHALL_USER/.ssh
cat /etc/valhall/id_rsa.pub >> /home/$VALHALL_USER/.ssh/authorized_keys;

msgHeadline "Reboot";
reboot;
*/
