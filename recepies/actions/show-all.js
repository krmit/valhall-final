#!/usr/bin/node
/*
 *   Copyright 2019-2020 Magnus Kronnäs
 */
"use strict";

const colors = require("colors");
const process = require("process");
const fs = require("fs-extra");
const path = require("path");
const yaml = require("js-yaml");
const { ConnectionPool } = require('ssh-pool');
const { logger } = require("../../router/logger.js");

//const stdin = process.openStdin();
const log = new logger(7);

const program_dir = path.join(__dirname, "..");
const path_config = `${program_dir}/build/host-config.yaml`;

fs.readFile(path_config, { encoding: "utf-8" })
  .then(yaml.safeLoad)
  .then(config => {
	  log.trace(config);
      const pool = new ConnectionPool(['@'])
      return pool.run('hostname');
});
