#!/bin/sh
################################################################
# Author:  Magnus Kronnäs
# license: GNU GENERAL PUBLIC LICENSE Version 3
#
# Copyright (c) 2018-2019 Magnus Kronnäs
################################################################
set -e;

echo "Setup basic installation environment for Alpine";

echo "Install applications."
apk update
apk upgrade
apk add git
apk add sudo


echo "Create a user";
echo "Create a user with username: $1";
echo "Also set password, but not in a secure way.";
echo -e "$2\n$2\n" | adduser $1;

echo "Make user sudo";
echo "Create a group sudo";
addgroup sudo;
adduser $1 sudo;
echo "Give the sudo group sudo privileges";
echo '%sudo     ALL=(ALL:ALL) ALL' >> /etc/sudoers;

echo "Needed to remov waning message on Ubuntu";
echo "Set disable_coredump false" >> /etc/sudo.conf

echo "Set upp ssh";
echo "Install ssh";
apk add openssh;
echo "Update the rc bootloader so ssh server always will start on boot up.";
rc-update add sshd;
echo "Create ssh map";
mkdir /home/$1/.ssh;
chown $1:$1 /home/$1/.ssh;
echo "Copy public key for host to root user on guest.";
cat /etc/valhall/id_*.pub >> /home/$1/.ssh/authorized_keys;
chown $1:$1 /home/$1/.ssh/authorized_keys;

echo "Install node."
apk add nodejs
apk add npm

echo "Install node program."
npm install -g typescript

echo "Reboot."
reboot;
