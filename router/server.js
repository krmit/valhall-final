#!/usr/bin/node
/*
 *   Copyright 2019 Magnus Kronnäs
 *
 *   This file is part of mimer-router.
 *
 *   logger.js is free software: you can redistribute it and/or modify it under the
 *   terms of the GNU General Public License as published by the Free Software
 *   Foundation, either version 3 of the License, or (at your option) any later
 *    version.
 *
 *   easy-web-framework is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License along with
 *   mimer-router. If not, see http://www.gnu.org/licenses/.
 */
"use strict";

const express = require("express");
const colors = require("colors");
const process = require("process");
const fs = require("fs-extra");
const path = require("path");
const yaml = require("js-yaml");
const http = require("http");
const request = require('request');
const { logger } = require("./logger.js");
const proxy = require('http-proxy-middleware');

//const stdin = process.openStdin();
const log = new logger(7);

let database = {};
const program_name = "mimer-editor";
const program_dir = path.join(__dirname, "..");
const path_config = `${program_dir}/build/host-config.yaml`;

fs.readFile(path_config, { encoding: "utf-8" })
  .then(yaml.safeLoad)
  .then(config => {
    log.debug("The config for the program is loaded.");
    
    database = config.users;
    log.trace(database);
    config.port=443;
    config.mode={};
    config.mode.web=80;
    config.mode.http=80;
    config.mode.https=443;
    config.mode.editor=3000;
    for(let i=0; i < 10; i++) {
        config.mode["port"+i]=10000+i*1000;
    }
    
    log.info(config);

    function server_error(req, res) {
      res.status(404).send("Not Found: 404 :(\n");
    }

    const app = express();

    app.get("/", function(req, res) {
      server_error(req, res);
    });

    // For making log on all request.
    app.use(function(req, res, next) {
      log.web(req);
      next();
    });

//    app.use("/web", express.static(path.join(program_dir, "web/")));

for(let user_key in database) {
    for(let mode in config.mode) {
        app.use(
        `/${user_key}/${mode}`,
        proxy({ target: `http://${database[user_key]}:${config.mode[mode]}`, changeOrigin: true, pathRewrite: {[`/${user_key}/${mode}`]: '/' } 
			})
      );
    }
}

    /*app.get("/:user/:mode", function(req, res) {
      const user_key = req.params["user"];
      const mode_key = req.params["mode"];
      log.trace(user_key);
      log.trace(mode_key);
      if (database.hasOwnProperty(user_key) && config.mode.hasOwnProperty(mode_key)) {
        //res.send(database[user_key]);
         http.get(`http://${database[user_key]}:${config.mode[mode_key]}`, function(response) {
             console.log(response);
             res.write(response);
         }).on('end', function() {
        res.end();
        //req.pipe(request(`http://${database[user_key]}:${config.mode[mode_key]}`)).pipe(res);       
    });
      } else {
        server_error(req, res);
      }
    });*/

    app.listen(config["port"], () => {
      log.info("Started server on port: " + config["port"]);
      log.debug("Test on URL: http://127.0.0.1:" + config["port"]);
  });
});
