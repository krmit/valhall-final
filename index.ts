#!/usr/bin/node
/*
 *   Copyright 2019-2020 Magnus Kronnäs
 */

"use strict";
import { LoggerTerminal as log } from "@mimer/logger";
import { t } from "@mimer/text";
import yargs from "yargs";
import createUser from "./recepies/actions/create-user.js";
import hostSetup from "./recepies/profiles/host.js";
import userSetup from "./recepies/profiles/base.js";
import { load, mkdir } from "./lib/shelljs-extra.js";

let yarg = yargs({})
      .describe("o", "Setup a host")
      .alias("o", "host")
      .default("o", false)
      .describe("b", "Setup a base")
      .alias("b", "base")
      .default("b", false)
      .describe("c", "Create a user")
      .alias("c", "create-user")
      .nargs("c", 1)
      .default("c", "")
      .describe("do-it", "Give a verbose print")
      .default("do-it", false)
      .boolean(["do-it", "b", "o"])
      .epilog("copyright 2019-2020 Magnus Kronnäs");
      

const arg = log.yarg(yarg).argv;
log.parseArg(arg);
log.log();

log.trace(arg);

log.show(t("Program started").headline);
const config = load("./config.yaml");
mkdir("./build");

let do_it = false;
if(arg["do-it"]) {
  log.warn("We are doing it!");
  do_it = true; 
}

if(arg.host) {
  log.debug("Setup a host");
  hostSetup(config, do_it)
} 
else if(arg.createUser !== "") {
  log.debug("Creating a user");
  createUser(arg["create-user"], config, do_it)
}
else if(arg.base) {
  log.debug("Setup user");
  userSetup(config, do_it);
}
else {
  log.warn("Nothing to do")
  log.warn('Choose "host", "create" or "base"');
}
