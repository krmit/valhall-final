#  Todo

## !Fix all tools

## !Translate old script into new format

## !Descide new host platform

## Test all script in VM
  * !Make DoIt Work
  * !Implement tools
  * !Skpa en användare manuelt.
  * !Få en användare att ha nätverkskontakt manuellt.
  * !Test create user
  * !Test config script on user
  * !Test beeing a user
  * !Fix reroting ip and port. Få det fungera  manuellt.

## Make bash and company things work
  * !Få bash att installera.
  * !Redigera .bashrc
  * Make a program to show informaiton about user pretty printet and save in README.md.

## Make mimer editor work
  * !Installera programmet.
  * !Få programmet att fungera
  * !Installera programmet hos användaren.
  * !Gör en specifikport för det.
  * !Gör så att med använder en inställning för extern domän/url och port.
  * !Gör så denna port finns med i inställningen som skapas av programmet.
  * Leta efter bugar.

## Make ttyd work

## Make web routing work
  * !Bestäm vilka weburl som ska fungera.
  * !Programmera en expresserver som routrar länken.
  * !Gör så detta fungerar med med.
  * !Läs in en konfigurationsfil.

## Make it work on new platform
  * !Install container
  * !Make ttyd work
  * Make med work
  * Skriv klart uppgift utfrån det faktiska biblioteket.

# Så fort du får tid

## Gör backup
  * Fundera ut var lxc data är.
  * Gör en rsync request som kopiern ned alla containers.
  * Kopla den till ett cron jobb så det görs varje dag.

## Gör ett vektyg som kör ett kommando på alla host.
  * !Välj bibliotek. ssh-pool.
  * Skap ett delprojekt med npm package file.
  * Läs in konfig filen och utför ett kommando.
  * Läs in kommando ifrån cli.
  * Se till att utskriften blir okey.

## Undersök prestanda genom logar.

# Snar Framtid

## Gör så att all serverar är deamons

## Fixa ssl för allt
 * Få ssl work för router.
 * Gör så att ttyd programmet routas genom den.

## Fixa configurationerna
 * Fundera igenom vilka konfigurationsfiler som ska finnas.
 * Gör så dessa används och inga konfigureringar i kod.
 * Ha en extern map för konfigureringar. 

## Router
  * Gör något för statiska filer så de kan visas effektivt.
  * Gör så att en hemsida visa för index.html

## Börja använda ntf

## Gör installation script som är platofrms oberonde.
  * Skapa ett skript som ger namn på installations program.
  
## Gör en egen developmiljö med browser plus valhall.
  * Skapa en valhall server för utveckling.
  * Gör ett nytt guest skript för mina dev önskemål.

## Gör ett script för att skapa en egen hem account för desktop.

## Skriv om setup för host så det kan fungera för att setup en hel host.
  * Skapa skript för att bota upp systemet.

# Lägre fram

 
## Fix AppAmor
 * Varför fungerar det inte?

## Överför till mimer ramverket.

## Gör så allt blir som "steg" som kan sättas i hopp.
