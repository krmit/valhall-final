/*
 *   Copyright 2019-2020 Magnus Kronnäs
 */
"use strict";
import { update, upgrade, install, command, mkdir, wget } from "../../lib/shelljs-extra.js";

export default function setup(config:any) {
  const log = config.log;
    
  log.headline("Setup my computer");
  log.confirm();
  log.info("/w       Whitelist");
    update();
  upgrade();
  install("sudo");
  install("htop");
  install("openssh");
  install("nano");
  install("cowsay");
  install("bash");
  command("curl https://sh.rustup.rs -sSf | sh");
  command("cargo install bat");
  log.headline("Generating SSH keys");
  command("mkdir -p .ssh") 
  command("ssh-keygen -f .ssh/id_rsa -t rsa -N ''")
}

