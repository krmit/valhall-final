/*
 *   Copyright 2019-2020 Magnus Kronnäs
 */
"use strict";
import { command, update, upgrade, install, load, write, npm, doIt } from "../../lib/shelljs-extra.js";
import { LoggerTerminal as log } from "@mimer/logger";
import { t } from "@mimer/text";
const headline = (msg:string) => log.show(t("\n",t(msg).headline));

export default function setup(config:any, doit:boolean) {
  if(doit) {
	doIt(); 
  }
  
  headline("Setup a new user");
  headline("Install software");
  update();
  upgrade();
  install("htop");
  install("openssh");
  install("curl");
  install("nano");
  install("bash");
  install("bash-completion");
  install("tree");
  
  headline("Install npm software");
  npm("cowsay");


  headline("Create useful folders");
  log.info("Create dev folder for git projects.");
  command("mkdir ~/dev;");
  log.info("Create bin folder for user created or compiled programms");
  command("mkdir ~/bin;");
  log.info("Create config folder");
  command("mkdir -p ~/.config/");

  headline("Copy files")
  log.info("Copy bash config file");
  command("ln -s $HOME/app/valhall-final/data/config/bashrc $HOME/.bashrc;");
  log.info("Created profile file to load .bashrc");
  write("~/.profile", "source $HOME/.bashrc;\n");
  
  log.info("Copy micro config file");
  command("mkdir -p ~/.config/micro"); 
  command("cp /etc/valhall/bindings.json ~/.config/micro/");  
  
  command("cat $HOME/.profile");
  log.info("Change passwd file so bash is standard for user.");
  command("sudo sed -i -- '$s/\\/bin\\/ash/\\/bin\\/bash/g' /etc/passwd")

 /* headline("Install software from git");
  command("~/app/valhall-final/data/scripts/install-mimer-editor.sh");
  log.info("Copy med editor config file");
  command("mkdir -p ~/.config/mimer-editor/");
  command("cp /etc/valhall/editor-config.yml ~/.config/mimer-editor/config.yml;");*/

  headline("Create ssh keys.");
  log.info("Generate ssh keys");
  command('ssh-keygen -f ~/.ssh/id_rsa -q -P ""');
  
  headline("Reboot the system");
  log.warn("Reboot!");
  command("sudo reboot");
}
