/*
 *   Copyright 2019-2020 Magnus Kronnäs
 */
"use strict";
import { update, upgrade, install, command, mkdir, wget } from "../../lib/shelljs-extra.js";

export default function setup(config:any) {
  const log = config.log;
    
  log.headline("Make a backup");
  log.confirm();
  log.info("/w       Whitelist");
  command(`rsync -avz -e \"ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null\" --progress mangus.kronnas@minecraft.kronnas.se:/backup ./backups/minecraft.kronnas.se/`)
}

