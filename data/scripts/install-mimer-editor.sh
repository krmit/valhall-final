#!/bin/sh
################################################################
# Author:  Magnus Kronnäs
# license: GNU GENERAL PUBLIC LICENSE Version 3
#
# Copyright (c) 2018-2020 Magnus Kronnäs
################################################################
set -e;

echo "Setup basic installation environment for Alpine";
cd $HOME/app

echo "Clone with git."
git clone https://gitlab.com/krmit/mimer-editor-pre.git;
cd mimer-editor-pre;
echo "Install with npm."
npm install
npm audit fix
echo "Link to binary"
ln -s $HOME/app/mimer-editor-pre/editor.js $HOME/bin/med 
ln -s $HOME/app/mimer-editor-pre/server/server.js $HOME/bin/med-server

