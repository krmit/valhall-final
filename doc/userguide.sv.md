# Valhall

Välkommen till Valhall server. En plattform för att lära sig hantera en server. Detta dokument innehåller information, tips och förmaningar om systemet. Läs det som är relevant för dig.

## Tillkortkommanden

  * Systemet kan vara instabilt och vi skulle kunna förlora all data. Ta därför och gör backup på vad du gör.
  * Tänk bara på att systemet för tillfället inte begränsar användandet av systemet, så du måste vara försiktig så du inte överbelastar servern.
  * Systemet använder inte SSL, vilket gör att all kommunikation via webben är okrypterad. Med andra ord om du använder webb terminalen kommer ditt lösenord skicka i klartext över internet. Därför använd ett unikt lösenord för din server som du inte använder någon annanstans.

## Logga in på Valhall

Fråga närmaste Valkyria eller be krm att skapa dig som användare i systemet. När det är klart kan du logga in med ssh vilket är installerat på de flesta datorerna. Använd följande kommando:

```
ssh -p *MinPort*  *AnvändarNamn*@valhall.htsit.se
```

Skriv in *Lösenord* för att du ska logga in i systemet. 

Du ska ha fått *Lösenord*, *MinPort* och *AnvändarNamn* av den som skapade kontot åt dig på valhall.

### Webb terminal

Om du inte har någon egen terminal med en ssh klient eller porten du ska använda är spärrad av någon brandvägg så kan du använda web terminalen. Gå in på sidan (valhall.htsit.se)[valhall.htsit.se] och du kommer få en terminal i din webbläsare. Du skriver bara ditt användarnamn och sedan lösenord. OBS! Denna kommunikation är inte krypterad, var därför försiktig.

### Windows

Om du på grund av allmän förvirring använder windows kan du med fördel använda dig av “putty” programmet. Ladda ned det från deras (hemsida)[http://www.chiark.greenend.org.uk/~sgtatham/putty/download.html].

Det är bara en fil så den är lätt att installera även om du skulle sakna alla behörigheter på ditt system. Putty är lättanvänt, skriv bara in *AnvändarNamn*@valhall.htsit.se som domän och *MinPort* som port. 

## Ändra lösenord

*!OBS! Bytt lösenord* 

Glöm inte att ändra lösenord! Skriv följande kommando för att ändra lösenord och följ instruktionerna:

```
passwd
```

## Använda Linux

Du får nu lite allmän info via terminalen läs den gärna det kan stå något intressant. Annars har du ett linux shell som du kan leka med.

När du loggar in kommer alla dina kommandon som du skriver tolkas av ett program som heter bash, det kallas för ditt “shell”. Du kan skriva in de flesta Linux/UNIX kommandon och de kommer fungera. Tänk bara på att vara varsam. Om du vill redigera en fil kan du använda programmet micro som editor, alternativ kan du använda vim eller emacs men du måste installera dem själv i så fall. 

### Micro

*micro* är en relativt ny editor,  men som har den stora fördelen att den går att använda som nästan vilket annat skrivbordsprogram som helst. Du kan använda Ctrl-S för att spara och Ctrl-C för att kopiera med mera.

Två saker är dock värda att notera:

  * Urklipp kommer bara fungera i programmet, inte i andra program på ditt skrivbord. I visa terminaler kan du klistra in utifrån genom kommandot “Ctrl-Shift-V”.
  * I webb terminalen kan du inte använda “Ctrl-Q” för att stänga av programmet eftersom browser stjäl detta kortkommando. Använd “F1” istället för att stänga av programmet. 

### Vim

Vim är text editor som anses vara svår att lära sig, men den är rätt så enkel om du bara vill lära dig några grundläggande kommandon. Skriv bara vim följd av namnet på filen du vill öppna. 

i    Gör att du går in i “input mod” och du kan nu skriva text som i en vanlig editor.
Esc    Gör så att du lämnar “input mode” och du kan nu skriva kommandon som vim tolkar.
:wqEnter    Gör så att du sparar alla ändringar i en filen och avslutar programmet.

Några andra roliga kommandon:

dd    Tar bort en rad och gör så att den kan kopieras.
p    Klistrar in det som ska kopieras
u    undo
v    Markera det du vill i “visual mode”. Avslutas genom att trycka “y” och markeringen kopieras då.

Det finns extremt många fler kommandon att lära sig om du vill! 
 

## Åtkomst till filsystem

Ett enkelt sätt att hantera din server är att ansluta dig direkt till serverns filsystem. Det vill säga montera det på filsystemet på det operativsystem som du kopplar upp dig ifrån. Detta är enkelt på Unix.

På din dator skapa en mapp “www” i din hemkatalog, eller något liknande. Använd sedan följande kommando för att ansluta till servern, glöm inte skriva lösenord.

```
sshfs -p *MinPort*  *AnvändarNamn*@valhall.htsit.se ~/www/
```

Nu har du hela ditt projekt i mappen “www” och kan göra ändringar där precis som om filerna var på din egen dator. 

### Windows

Använder du windows är det nog enklaste sättet att använda notepad++. OBS! Det kan innebära att du måste ha adminrättigheter på din dator.

Ladda ned notepad++ med plugins och som en zip fil direkt från deras hemsida. Gå sedan in och välj ftp pluginen. Ange AnvändarNamn@valhall.htsit.se som domän och *MinPort* som port. Du kan nu editera dina filer direkt i din dator via notepad++. 

## Linux Alpine

Den linux distribution vi använder oss av heter Linux Alpine. Det är en mycket liten och effektiv distribution och passar därför till en server med liten prestanda. 

En egenhet är att Alpine använder ett annat clib än som är vanligt, det vill säga att det mest grundläggande programmering biblioteket är har en annan implementering. Därför kan det ibland vara svårt att köra program från andra linux distribution på Alpine, men oftast finns programmet i pakethanteraren heller kan du kompilera det själv.

Ett annan skillnad mot de flest moderna distributioner är att Alpine inte använder systemd för processhantering och initialisation av systemet. Istället används det enklare och mer traditionella ramverket OpenRC.  

## Installera

Alpines pakethanteraren är mycket lik andra pakethanteraren såsom apt. För att installera ett *program* ge kommandot:

```
apk add program
```

## Installerade program 

Det finns flera program som redan är installerade och som rekommenderas att användas om det behövs. En del av dem kan behövas i olika inlämningsuppgifter.

### Typescript

Kompilator för typescript. Du kan kompilera alla typescript kod i ett projekt genom att skriva koden nedan.

```bash
tsc
```

## Rangarrök

Det kommer hända att systemet nollställs och bara de som så begär kommer få nya inloggningar. Detta borde ske i mitten av juni när terminen är slut. Var beredd på detta och kopiera saker som du vill ha kvar till din dator.



