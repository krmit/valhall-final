#!/bin/sh
################################################################
# Author:  Magnus Kronnäs
# license: GNU GENERAL PUBLIC LICENSE Version 3
#
# Copyright (c) 2018-2019 Magnus Kronnäs
################################################################
set -e;

echo "Prepare user account"
cd;
mkdir app;
cd app;

echo "Install valhall."
git clone https://gitlab.com/krmit/valhall-final.git
echo "Start npm installation."
cd valhall-final
npm set registry http://htsit.se:4873
npm install
tsc
echo "Ready to setup user."
node dist/index.js -b --do-it

