/*
 *   Copyright 2019 Magnus Kronnäs
 */
"use strict";
import { password, load, save, command, doIt } from "../../lib/shelljs-extra.js";
import colors from "chalk";
import * as moment from 'moment';
import * as figlet from "figlet";

import { LoggerTerminal as log } from "@mimer/logger";
import { t } from "@mimer/text";
const headline = (msg:string) => log.show(t("\n",t(msg).headline));

export default function createUser(username:string, config:any, doit:boolean) {
  if(doit) {
	doIt(); 
  }
  
  headline("Create user");
  log.print("Create user with username: " + username);
  const host_path="./build/host-config.yaml";
  const host_config = load(host_path);
  
  headline("You must give a password for the user");
  const user_password = password("Give me a good password: ");
  
  headline("Set options for new user");
  const guest_config:any = {};
  guest_config.alias = username;
  guest_config.id = host_config.next_user_id;
  host_config.next_user_id = host_config.next_user_id+1;
  guest_config.interface = "eth0";
  log.print("Id for the user will be: " + colors.green(guest_config.id));
  guest_config.image = config.valhall.lxc_image;
  log.print("The image will be: " + colors.yellow(guest_config.image));
  let ip_parts = config.network.ip_start_on.split(".")
  let ip_number = Number(ip_parts.pop())+guest_config.id;
  guest_config.ip = ip_parts.join(".")+"."+ip_number;
  log.print("The ip on the bridge will be: " + colors.green(guest_config.ip));
  guest_config.port = config.network.ports_start_on + guest_config.id*100;
  log.print("The open ports will begin with: " + colors.green(guest_config.port));
  headline("Decide all ports number");
  guest_config.ports = {};
  guest_config.ssh = guest_config.port+22;
  guest_config.ports[22] = guest_config.ssh;
  log.print("The http ports will be: " + colors.green(guest_config.ssh));
  guest_config.http = guest_config.port+80;
  guest_config.ports[80] = guest_config.http;
  log.print("The ssh ports will be: " + colors.green(guest_config.ssh));
  guest_config.https = guest_config.port+43;
  guest_config.ports[443] = guest_config.https;
  log.print("The http ports will be: " + colors.green(guest_config.http));
  guest_config.med_port = guest_config.port+30;
  guest_config.ports[3000] = guest_config.med_port;
  log.print("The port for editor med will be: " + colors.green(guest_config.http));
  for(let i = 0; i < 10; i++) {
      guest_config["port"+i] = guest_config.port+10+i;
      let port = 10000+i*1000;
      guest_config.ports[port] = guest_config["port"+i];
      log.print("The your own port "+ i+ " will be: " + colors.green(guest_config["port"+i]));
  }
  log.confirm();
  headline("Save config for the new guest.");
  console.log(guest_config);   
  save("./build/"+guest_config.alias+"-guest-config.yaml", guest_config);
  
  headline("Create a new container for user "+guest_config.alias);
  log.info(`Create a new container for user ${guest_config.alias} with image ${guest_config.image}`);
  command(`sudo lxc init images:${guest_config.image} ${guest_config.alias}`);
  log.info(`Remove demand for AppArmor since difficult to get it working`);
  command(`sudo lxc config set ${guest_config.alias} raw.lxc "lxc.apparmor.allow_incomplete = 1"`);
  log.info(`Conect it to bridge ${config.network.brige} on interface ${guest_config.interface}`);
  command(`sudo lxc network attach ${config.network.brige} ${guest_config.alias} ${guest_config.interface};`);
  log.info(`Set ip on devise to ${guest_config.ip}`);
  command(`sudo lxc config device set ${guest_config.alias} ${guest_config.interface} ipv4.address ${guest_config.ip}`);
  log.info(`Start container`);
  command(`sudo lxc start ${guest_config.alias}`);

  headline("Forward a port to ssh port on user container");
  log.warn("iptables should change to nft")
  
  for(const key of Object.keys(guest_config.ports)) {
    command(`sudo iptables -t nat -A PREROUTING -i ${config.network.interface} -p tcp --dport ${guest_config.ports[key]} -j DNAT --to ${guest_config.ip}:${key}`)
  }
  
  headline("Create config for editor");
/*  const editor_config:any={};
  editor_config.domain="valhall.htsit.se";
  editor_config.externalPort=443;
  editor_config.searchPath=`/${guest_config.alias}/editor`;
  save("./build/editor-config.yml", editor_config);
  */
  headline("Copy files to container.")
  log.info("Create map for valhals files");
  command(`sudo lxc exec ${guest_config.alias}  -- /bin/ash -c \"mkdir -p /etc/valhall/\";`);
  log.info("Copy boot script to container.");
  command(`sudo lxc file push ./data/scripts/boot-setup-alpine.sh ${guest_config.alias}/etc/valhall/`);
  log.info("Copy boot script for user.");
  command(`sudo lxc file push ./data/scripts/boot-user-alpine.sh ${guest_config.alias}/etc/valhall/`);
  //log.info("Copy config for editor.");
 // command(`sudo lxc file push ./build/editor-config.yml ${guest_config.alias}/etc/valhall/editor-config.yml`);
  log.info("Copy config file to container.");
  command(`sudo lxc file push ./build/${guest_config.alias}-guest-config.yaml ${guest_config.alias}/etc/valhall/guest-config.yaml`);
  log.info("Copy public ssh key.");
  command(`sudo lxc file push  ~/.ssh/id_*.pub ${guest_config.alias}/etc/valhall/`);
  
  log.info("Copy binary program of editor micro.");
  command(`sudo lxc file push  ./data/bin/micro ${guest_config.alias}/usr/bin/`);
  log.info("Copy config for micro editor.");
  command(`sudo lxc file push  ./data/config/micro/bindings.json ${guest_config.alias}/etc/valhall/`);

  headline("Save updated config for the host.");
  host_config.users[guest_config.alias]=guest_config.ip;
  console.log(guest_config.alias);
  save(host_path,host_config);
  
  headline("Create message of to day.");

  let motd = colors.bold.red("Welcome to\n");
  motd += colors.bold.cyan(figlet.textSync(guest_config.alias, {font: 'Banner'}));
  motd += "\n";
  motd += colors.bold.cyan(figlet.textSync("server", {font: 'Banner'}));
  motd +=colors.bold.red("\n on\n");
  motd += colors.bold.blue(figlet.textSync("Valhall host 0.4", {font: 'Slant'}));
  motd += "\n";
  motd += colors.bold.underline("Gör inget dumt! Läs användar vilkoren.\n");
  motd += colors.bold.underline("https://htsit.se/valhall\n");
  motd += "\n";
  motd += colors.bold("Created: ") + "["+colors.green(moment().format("YYYY-MM-DD HH:mm:ss"))+"]\n";
//  motd += colors.bold("Last login: ") + "["+colors.green("`date`")+"]\n";
//  motd += colors.bold("CPU: ") + "["+colors.green("$CPU")+"]                    ";
//  motd += colors.bold("Memory: ") + "["+colors.green("$MEMORY")+"]\n";
//  motd += colors.bold("Bandwidth total: ") + "["+colors.green("$BANDWIDTH")+"]";
  

  log.print(motd);
  log.info("Save motd");
  save("./build/motd.txt",String(motd));
  log.info("Copy motd to guest");
  command(`sudo lxc file push ./build/motd.txt ${guest_config.alias}/etc/motd`);
  headline("Run setup script on user guest");
  log.info("Do boot stuff as root");
  command(`sudo lxc exec ${guest_config.alias} -- /bin/ash -c "cd /etc/valhall/;./boot-setup-alpine.sh ${guest_config.alias} ${user_password};"`);
  log.info("Waite on reboot.");
  command("sleep 10");
  log.info("Do boot stuff as user");
  command(`echo "${user_password}" | ssh -o "StrictHostKeyChecking no" -tt ${guest_config.alias}@${guest_config.ip} /etc/valhall/boot-user-alpine.sh`);
  headline("User is created!");
}
