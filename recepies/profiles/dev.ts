/*
 *   Copyright 2019-2020 Magnus Kronnäs
 */
"use strict";
import { command, update, upgrade, install, cargo, load, npm, doIt } from "../../lib/shelljs-extra.js";

export default function setup(config:any, password:string, doit:boolean) {
  if(doit) {
	doIt(); 
  }
  const log = config.log;
  
  log.headline("Setup a new user");
  log.log("Read config");
  const guest_path="/etc/valhall/guest-config.yaml";
  const guest_config = load(guest_path);
  log.headline("Install software");
  update();
  upgrade();
  
  log.headline("Install C compiler");
  install("gcc");
  install("make");
  install("automake");
  install("cmake");
  install("g++");

  log.headline("Install rust");
  install("rust");
  
  log.headline("Install editors");
  install("vim");
  
  log.headline("Install important tools");
  install("imagemagick");
  cargo("bat");
  npm("js-yaml");
  npm("github-markdown");
  
  log.headline("Install fun tools");
  install("sl");
  
  




  
  
}
