/*
 *   Copyright 2019-2020 Magnus Kronnäs
 */
"use strict";
import {update, upgrade, install, command, save, doIt } from "../../lib/shelljs-extra.js";

import { LoggerTerminal as log } from "@mimer/logger";
import { t } from "@mimer/text";
const headline = (msg:string) => log.show(t("\n",t(msg).headline));

export default function setup(config:any, doit:boolean) {
  if(doit) {
	doIt(); 
  }
  
  headline("Set up a new server");
  log.warn(
    "This script is more a reminder of how the server was configured, should not be used."
  );
  log.confirm();
  headline("Install software");
  update();
  upgrade();
  install("lxc");
  install("lxd");
  
  headline("Mount cgroup - Do not know if it is necessary.");
  log.info("Check if a mounted cgroups exists");
  let flag = command("mountpoint -q /sys/fs/cgroup");
  if(flag === false) {
	  log.info("Mount cgroups, not mountet by default in alpine");
	  command("sudo mount -t cgroup cgroup /sys/fs/cgroup");
  }
  headline("Create network brig");
  log.info("Answers should be no,yes, valhall-pool, dir, no, no, no, no, yes, yes");
  command("sudo lxd init")
  command(`sudo lxc network create ${config.network.brige} ipv6.address=none ipv4.address=${config.network.brige_ip}/24 ipv4.nat=true`);
  headline("Create configurattion for host.")
  
  headline("Make network reddy for forwarding");
//  log.warn("iptables should change to nft")
//  command("sudo sh -c 'echo 1 > /proc/sys/net/ipv4/ip_forwar'");
  
  let host_data:any = {};
  log.info("Find start number for Valhall users");
  log.info(config.valhall.user_id_start_on);
  host_data.next_user_id=config.valhall.user_id_start_on;
  host_data.users={};
  log.info("Save configuration for host");
  save(config.build.path+"/host-config.yaml", host_data);
  command("mkdir ~.ssh");
  command("ssh-keygen -f ~/.ssh/id_rsa -t rsa -N ''");
  
  headline("Install ttyd from git");
  command("cd ~/dev")
  command("sudo apt-get install cmake g++ pkg-config git vim-common libwebsockets-dev libjson-c-dev libssl-dev");
  command("git clone https://github.com/tsl0922/ttyd.git");
  command("cd ttyd && mkdir build && cd build");
  command("cmake ..");
  command("make")
  command("sudo make install");
  
  headline("Config and start");
  command("cd ~/dev/valhal-final")
  //  command("sudo useradd forttyd");
  //  command("sudo adduser forttyd sudo");
  //  command("sudo su forttyd");
  command("tmux new -s ttyd")
  command("ttyd -p 80 ./data/scripts/login-to-container.sh");  
}
