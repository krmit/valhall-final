#!/bin/sh
# This only a draft. Do not use it as a script.
set -e;
mkdir app
cd app
curl https://nodejs.org/dist/v14.8.0/node-v14.8.0-linux-x64.tar.xz
mv node-v12.18.3-linux-x64/ node
echo "export PATH=\$HOME/app/node/bin:\$PATH" >> ~/.bashrc

sudo apt install update
sudo apt install upgrade
sudo apt install ssh
sudo apt install xclip

ssh-keygen -t ed25519 -C "asgard"
xclip -sel clip < ~/.ssh/id_ed25519.pub

sudo apt install git
sudo apt install figlet
sudo apt install tput
sudo apt install wmctrl
sudo apt install xdotools
