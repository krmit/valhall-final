#!/usr/bin/node
/*
 *   Copyright 2019-2020 Magnus Kronnäs
 */
"use strict";

const colors = require("colors");
const process = require("process");
const fs = require("fs-extra");
const path = require("path");
const yaml = require("js-yaml");
const { ConnectionPool } = require('ssh-pool');
import Logger from "logger";

const logger = new Logger();
let log = logger.log({});

//const stdin = process.openStdin();
const log = new logger(5);

const alias = process.argv[2];
const url = "http://valhall.htsit.se/";

log.trace(alias);

const program_dir = path.join(__dirname, "..");
const path_config = `${program_dir}/build/${alias}-guest-config.yaml`;

fs.readFile(path_config, { encoding: "utf-8" })
  .then(yaml.safeLoad)
  .then(config => {
	  log.trace(config);
      console.log(colors.green("Alias:      ")+colors.green.bold(config.alias));
      console.log(colors.green("Id:         ")+colors.green.bold(config.id));
      console.log(colors.green("Valhall IP: ")+colors.green.bold(config.ip));
      console.log("\n"+colors.green.underline("Port")+"\n");
      console.log(colors.green("Base port:  ")+colors.green.bold(config.port));
      console.log(colors.green("ssh:        ")+colors.green.bold(config.ssh));
      console.log(colors.green("http:       ")+colors.green.bold(config.http));
      console.log("\n"+colors.green.underline("Länkar")+"\n");
      console.log(colors.green("ssh:        ")+colors.green.bold(url));
      console.log(colors.green("http:       ")+colors.green.bold(url+alias+"/http");
});
