/*
 *   Copyright 2019 Magnus Kronnäs
 *
 *   This file is part of easy-web-framework.
 *
 *   This program is free software: you can redistribute it and/or modify it under the
 *   terms of the GNU General Public License as published by the Free Software
 *   Foundation, either version 3 of the License, or (at your option) any later
 *    version.
 *
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License along with
 *   easy-web-framework. If not, see http://www.gnu.org/licenses/.
 */
"use strict";
import logger from "./lib/logger.js";
import createUser from "./host/create-user.js";
import hostSetup from "./host/setup.js";
import userSetup from "./guest/setup.js";

const log = new logger(7);

log.log("Program started");

const commands = process.argv.slice(2);
log.trace(commands);
let command = commands.shift();
switch (command) {
  case "host":
    log.trace("Host");
    command = commands.shift();
    switch (command) {
      case "create":
        log.debug("Create user");
        command = commands.shift();
        createUser(command,config);
        break;
      case "setup":
        log.debug("Setup server");
        hostSetup();
        break;
      default:
        log.warn('Choose "create" or "setup"');
    }
    break;
  case "guest":
    log.trace("Guest");
    command = commands.shift();
    switch (command) {
      case "setup":
        log.debug("Setup user");
        userSetup();
        break;
      default:
        log.warn('Choose "setup"');
    }
    break;
  default:
    log.warn('Choose "host" or "guest"');
}
