#!/bin/sh
################################################################
# Author:  Magnus Kronnäs
# license: GNU GENERAL PUBLIC LICENSE Version 3
#
# Copyright (c) 2019 Magnus Kronnäs
################################################################
set -e;

echo "Username: ";
read USER;
MY_LXC_IP=`cat ./build/host-config.yaml | grep -w $USER | cut -d":" -f2 | tr -d " "`;
echo $USER@$MY_LXC_IP;
ssh -o PreferredAuthentications=password -o PubkeyAuthentication=no $USER@$MY_LXC_IP;
