/*
 *   Copyright 2019-2020 Magnus Kronnäs
 */
"use strict";

import * as shelljs from "shelljs";
import * as fs from "fs-extra";
import * as yaml from "js-yaml";
import * as consolePrompt from "prompt-sync";
import * as os from "os";
import * as process from "process";
import { LoggerTerminal as log } from "@mimer/logger";

let do_it = false;

export function doIt() {
    do_it = true;
}

const console_prompt = consolePrompt();

export function command(...commands:string[]):any {
  log.print(commands.join(" "));

  if(do_it) {
     let output = shelljs.exec(commands.join(" "));
     if(output.code === 0) {
       return output.stdout;
     } else {
	   log.error(output.stderr);
       return false;
     } 
    }
  return false;
}

export function install(app:string) {
  log.info("Installing " + app);
  command("sudo apk add", app);
}

export function cargo(app:string) {
  log.info("Installing rust app " + app);
  command("cargo install", app);
}

export function npm(app:string) {
  log.info("Installing node  app " + app);
  command("sudo npm install -g ", app);
}

export function prompt(ask:string) {
  return console_prompt(ask);
}

export function password(ask:string) {
  return console_prompt(ask, {echo: "*"});
}

export function update() {
  log.info("Update");
  command("sudo apk update");
}

export function load(file:string) {
  log.info("Load "+file);
  let data = fs.readFileSync(file, "utf8");
  return yaml.safeLoad(data);
}

function expand(path:string):string {
	return path.replace(/%([^%]+)%/g, (_,n) => process.env[n]  || '').replace(/^~(?=$|\/|\\)/,os.homedir());
}

export function save(file_path:string, data:any) {
  log.info("Save to " + expand(file_path));
  let data_text:string;
  if(typeof data === "string") {
	  data_text = (data as string);
  } else {
     data_text = yaml.safeDump(data);
 }
  log.trace("\n"+data_text);
  log.trace("\n"+do_it);
  if(do_it) {
	  log.info("Save!!!!!");
    fs.writeFileSync(expand(file_path), data_text);
  }
}

export function write(file_path:string, data:any) {
  log.info("Write to " + expand(file_path));
  let data_text:string;
  data_text = data.toString();
  log.trace("\n"+data_text);
  log.trace("\n"+do_it);
  if(do_it) {
	  log.info("Write!!!!!");
    fs.writeFileSync(expand(file_path), data_text);
  }
}

export function upgrade() {
  log.info("Upgrade");
  command("sudo apk upgrade");
}

export function ls(options:string[]) {
  log.info("ls " + options.join());
  if(do_it) {
      shelljs.ls(options);
  }
}

export function mkdir(map:string) {
  log.info("Create map if not exsist " + map);
  if(do_it) {
      shelljs.mkdir(map);
  }
}

export function wget(map:string) {
  log.info("Create map if not exsist " + map);
  if(do_it) {
       log.error("Not implementet.")
      //shelljs.wget(map);
  }
}
