/*
 *   Copyright 2019-2020 Magnus Kronnäs
 */
"use strict";
import { update, upgrade, install, command, mkdir, wget } from "../../lib/shelljs-extra.js";

export default function setup(config:any) {
  const log = config.log;
    
  log.headline("Set up a new minecradt server");
  log.confirm();
  log.headline("Install needed programs");
  update();
  upgrade();
  install("default-jdk");
  install("tmux");
  log.headline("Creat a sesion for minecraft server");
  command("tmux new -s minecraft")
  log.headline("Download minecraft server");
  mkdir("minecraft");
  wget("https://launcher.mojang.com/v1/objects/3737db93722a9e39eeada7c27e7aca28b144ffa7/server.jar");
  command("echo 'eula=true' > eula.txt");
  log.headline("Start minecraft server");
  command("java -Xmx1024M -Xms1024M -jar minecraft_server.jar nogui");
  log.headline("Some information about minecraft server");
  log.info("Commands for tmux");
  log.important("C-b d    Attach, leve server but server still runing.");
  log.info("C-b d    Attach, leve server but server still runing.");
  log.info("tmux a   Attach to last session, you are back to your server again.");
  log.info("Commands in mindecraft server");
  log.info("/w       Whitelist");
}

